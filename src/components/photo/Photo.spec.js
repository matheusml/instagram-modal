import React from 'react';
import renderer from 'react-test-renderer';

import Photo from './Photo';

test('Snapshot', () => {
  const component = renderer.create(<Photo />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});