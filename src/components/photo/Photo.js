import React from 'react';
import styled from 'styled-components';

import Avatar from '../avatar/Avatar';
import Comments from '../comments/Comments';

const Wrapper = styled.div`
    margin: auto;
    overflow: auto;
`;

const Image = styled.img.attrs({
    src: 'https://cdn.vox-cdn.com/uploads/chorus_image/image/49958705/game-of-thrones-poster_85627-1920x1200.0.jpg',
    alt: 'Game of thrones scene',
}) `
    width: 585px;
    height: 490px;
    display: block;
    float: left;
    padding-right: 15px;
`;

const Description = styled.section`
    background-color: #fff;
    padding: 10px 0;
`;

const Header = styled.header`
    height: 40px;
    padding: 15px;
    float: right;
    width: 280px;
    border-bottom: 1px solid #efefef;
`;

const Photo = () => (
    <Wrapper>
        <Image />
        <Description>
            <Header>
                <Avatar />
            </Header>
            <Comments />
        </Description>
    </Wrapper>
);

export default Photo;