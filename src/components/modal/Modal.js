import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Wrapper = styled.div.attrs({
    id: 'modal',
}) `
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: rgba(0, 0, 0, 0.5);
    transition: opacity 200ms;
    visibility: hidden;
    opacity: 0;
    &:target {
        visibility: visible;
        opacity: 1;
    }
`;

const Cancel = styled.a.attrs({
    href: '#',
}) `
    position: absolute;
    width: 100%;
    height: 100%;
    cursor: default;
`;

const Content = styled.article`
    margin: 100px auto;
    background: #fff;
    border: 1px solid #666;
    max-width: 935px;
    box-shadow: 0 0 50px rgba(0, 0, 0, 0.5);
    position: relative;
`;

const Modal = ({ children }) => (
    <Wrapper>
        <Cancel />
        <Content>
            {children}
        </Content>
    </Wrapper>
);

Modal.propTypes = {
    children: PropTypes.object.isRequired,
};

export default Modal;
