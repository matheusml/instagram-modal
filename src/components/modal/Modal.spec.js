import React from 'react';
import renderer from 'react-test-renderer';

import Modal from './Modal';

test('Snapshot', () => {
  const component = renderer.create(<Modal><h1>inside modal</h1></Modal>);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});