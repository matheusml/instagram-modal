import React from 'react';
import renderer from 'react-test-renderer';

import Comments from './Comments';

test('Snapshot', () => {
  const component = renderer.create(<Comments />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});