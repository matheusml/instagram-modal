import React from 'react';
import styled from 'styled-components';

import AddComment from './AddComment';
import CommentListItem from './CommentListItem';

const Wrapper = styled.section`
    height: 360px;
    width: 335px;
    overflow: auto;
    word-wrap: break-word;
    border-bottom: 1px solid #efefef;
`;

const CommentsList = styled.ul`
    list-style-type: none;
    -webkit-margin-end: 20px;
    -webkit-padding-start: 20px;
`;

const Comments = () => (
    <div>
        <Wrapper>
            <CommentsList>
                <CommentListItem text={'Valar Morghulis'} />
                <CommentListItem text={'nice pic ❤'} />
                <CommentListItem text={'Valar Dohaeris" is High Valyrian, the response phrase to "Valar Morghulis," the title of the Season 2 finale. Commonly used in Braavos, "valar morgulis" means "all men must die" (in the sense of "all men must [eventually] die [sooner or later]"), and "valar dohaeris" means "all men must serve."'} />
                <CommentListItem text={'great photo. awesome.'} />
                <CommentListItem text={'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum'} />
            </CommentsList>
        </Wrapper>
        <AddComment />
    </div>
);

export default Comments;