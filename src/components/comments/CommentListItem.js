import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import AvatarName from '../avatar/AvatarName';

const Wrapper = styled.li`
    padding: 5px 0;
`;

const CommentListItem = ({ text }) => (
    <Wrapper>
        <AvatarName />{text}
    </Wrapper>
);

CommentListItem.propTypes = {
    text: PropTypes.string.isRequired,
};

export default CommentListItem;