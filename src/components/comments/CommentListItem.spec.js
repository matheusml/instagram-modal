import React from 'react';
import renderer from 'react-test-renderer';

import CommentListItem from './CommentListItem';

const props = {
    text: 'my comments',
};

test('Snapshot', () => {
  const component = renderer.create(<CommentListItem {...props} />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});