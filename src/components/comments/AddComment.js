import React from 'react';
import styled from 'styled-components';

const Form = styled.form`
    margin-top: 15px;
`;

const Textarea = styled.textarea.attrs({
    placeholder: 'Add a comment...',
}) `
    height: 18px;
    background: 0 0;
    border: none;
    color: #262626;
    font-size: inherit;
    outline: none;
    padding: 0;
    resize: none;
    max-height: 80px;
    line-height: 18px;
    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Helvetica,Arial,sans-serif;
`;

const AddComment = () => (
    <Form>
        <Textarea placeholder="Add a comment..." />
    </Form>
);

export default AddComment;