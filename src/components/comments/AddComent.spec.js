import React from 'react';
import renderer from 'react-test-renderer';

import AddComment from './AddComment';

test('Snapshot', () => {
  const component = renderer.create(<AddComment />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});