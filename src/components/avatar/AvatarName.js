import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.a.attrs({
    href: 'https://www.instagram.com/instagram/',
    target: '_blank',
}) `
    font-weight: 600;
    color: #262626;
    text-decoration: none;
    display: inline-block;
    padding-right: 5px;
    &:hover {
        opacity: 0.7;
    }
`;

const AvatarName = () => (
    <Wrapper>instagram</Wrapper>
);

export default AvatarName;