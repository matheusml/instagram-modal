import React from 'react';
import renderer from 'react-test-renderer';

import Avatar from './Avatar';

test('Snapshot', () => {
  const component = renderer.create(<Avatar />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});