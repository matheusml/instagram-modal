import React from 'react';
import styled from 'styled-components';

import AvatarName from './AvatarName';

const AvatarImageWrapper = styled.a.attrs({
    href: 'https://www.instagram.com/instagram/',
    target: '_blank',
}) `
    color: #003569;
    text-decoration: none;
    float: left;
    padding-right: 15px;
    &:hover {
        opacity: 0.7;
    }
`;

const AvatarImage = styled.img.attrs({
    src: 'https://scontent-gru2-1.cdninstagram.com/t51.2885-19/s150x150/14719833_310540259320655_1605122788543168512_a.jpg',
    alt: 'Avatar',
}) `
    border-radius: 50%;
    border: solid 1px #ccc;
    width: 40px;
`;

const AvatarNameWrapper = styled.div`
    padding-top: 12px;
`;

const Avatar = () => (
    <div>
        <AvatarImageWrapper>
            <AvatarImage />
        </AvatarImageWrapper>
        <AvatarNameWrapper>
            <AvatarName>instagram</AvatarName>
        </AvatarNameWrapper>
    </div>
);

export default Avatar;