import React from 'react';
import Modal from './components/modal/Modal';
import Photo from './components/photo/Photo';

const App = () => (
  <div>
    <a href="#modal" className="button">Open Modal</a>
    <Modal>
      <Photo />
    </Modal>
  </div>
);

export default App;
