Instagram Modal
============

### Introduction

This project was made with:
- React (with [create-react-app](https://github.com/facebookincubator/create-react-app))
- [styled-components](https://github.com/styled-components/styled-components)
- [Jest](https://facebook.github.io/jest/) 

Since the components are very simple and do not contain any [state](https://facebook.github.io/react/docs/state-and-lifecycle.html), I'm testing them only with [Snapshots](https://facebook.github.io/jest/docs/snapshot-testing.html).

### Setup

To setup the project first install the dependencies:

```
yarn
```

or 

```
npm install
```

Then run the tests:

```
yarn test
```

or

```
npm test
```

And to see it running:

```
yarn start
```

or 

```
npm start
```

